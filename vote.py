import discord
import discord.ext.commands as commands
from random import choice

import base
from base import Cog

NEW_VOTE_TEXT = ["The pressure around the goblet is immeasurable. You know your community has a problem and it is up to you all to solve it, one vote at a time. Thus, once more, you have your paper ready, ready to throw it in among the others.",\
    "Dawn looms overhead and lights up the town with a golden glow. Today, one more person will be found guilty of murder, treason and eating the last cookie. Who will it be this time?",\
    "Crimson stains the floors as the blood of your friends flows freely along its crevices. These murders have to stop and you must be certain about who was the one that plunged the dagger into their hearts.",\
    "Executions are always grim; the wisps of death always looming. The methods are nasty, vile & inhumane. However, these deaths must be done by the hands of monsters. You cannot be humane when your enemy is not itself."\
    "The foul stench of rot permeates around the area. All these bodies piling up cannot be good for anything. Time to end this- with pitchfork in hand!"]

VOTED_TEXT = ["The accountant looks at you with a dry smile as he takes your ballot sheet. This person has to be the one who is causing all this damage.",\
    "No looking back- everyone else has now heard your words. You can only hope that they heed them as well.",\
    "No oath is stronger than the one committed to the crown. It now knows your choice and that of other gone before you.",\
    "Your painting is drying on the wall of the bakery. The might of a name, publicised for all to see, will leave the impact you long desired.",
    "The spirits accept your solemn offerings, judging your choice as yours alone. You beckon the next in line, who hold a bundle of grain ready as well."]

WRONG_VOTE_TEXT = "The pressure of your choice is immense, making you feel delusional for a split second. \n\ This is not a valid name, try again."

GUILD_ID = 711889630261149736
CHANNEL_ID = 714582729236152471

class Vote(Cog):

    def setup(self, config):
        self.current_vote = None
        self.current_targets = None

    @commands.command(name="startvote", hidden=True, rest_is_raw=True)
    @commands.has_permissions(administrator=True)
    @base.mutex_command
    async def startvote(self, ctx, *args):
        if ctx.guild is None:
            await ctx.send("Not allowed in DMs...")
            return
        if self.current_vote:
            await ctx.send("There is already an open vote")
            return
        await ctx.send("Send the names of the voters, line by line:")
        msg = await self.bot.wait_for('message', timeout=60, check = lambda m: m.author == ctx.author)
        lines = msg.content.split('\n')
        users = [discord.utils.find(lambda m: m.name.casefold() == x.casefold(), ctx.guild.members) for x in lines]
        if any(u is None for u in users):
            await ctx.send("Not all of the names provided are users")
            return
        for u in users:
            try:
                await u.send("-------------------\n"+choice(NEW_VOTE_TEXT)+"\n"+ '\n'.join([x.name for x in users]))
            except discord.errors.DiscordException:
                await ctx.send("Could not send DM to user " + u.name)
        await ctx.send("Voting has started!")

        self.current_vote = users
        self.current_targets = {}


    @commands.command(hidden=True, rest_is_raw=True)
    @commands.has_permissions(administrator=True)
    async def closevote(self, ctx, *args):
        if self.current_vote is None:
            await ctx.send("There is no vote!")
            return
        await ctx.author.send("Voting results:\n" + '\n'.join(("{} => {}".format(k.name,v.name) for (k,v) in self.current_targets.items())))
        self.current_vote = None
        self.current_targets = None

    @commands.command()
    async def register(self, ctx):
        role = ctx.guild.get_role(712039953327980574)
        await ctx.author.add_roles(role)
        await ctx.author.send("You have fallen down the rabbit hole, welcome in Wonderland!")

    @commands.Cog.listener()
    async def on_message(self, message):
        if  (not self.current_vote) or (message.author == self.bot) or \
            (not isinstance(message.channel, discord.DMChannel)) or \
            (message.author not in self.current_vote):
            return
        target = discord.utils.find(lambda u: u.name.casefold() == message.content.casefold(), self.current_vote)
        if target is None:
            await message.author.send(WRONG_VOTE_TEXT)
            return
        self.current_targets[message.author] = target
        await message.author.send(choice(VOTED_TEXT))
