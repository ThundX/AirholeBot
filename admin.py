from base import Cog, botadmin, post_embed, http
import discord
import discord.ext.commands as commands
import aiohttp
import os
from io import BytesIO
import zipfile

class Admin(Cog):

    def setup(self, config):
        pass

    @commands.command(pass_context=True, hidden=True)
    @commands.check(botadmin)
    async def restart(self, ctx):
        print("Restarting...")
        await ctx.send("Restarting...")
        await self.bot.logout()

    @commands.command(pass_context=True, hidden=True, rest_is_raw=True)
    @commands.check(botadmin)
    async def embed(self, ctx, channel_id:int, url='', *, text=''):
        await post_embed(self.bot.get_channel(channel_id), text, url)

    @commands.command(pass_context=True, hidden=True)
    @commands.check(botadmin)
    async def disable_command(self, ctx, command_name):
        try:
            self.bot.get_command(command_name).enabled = False
            await ctx.send("Command disabled")
        except AttributeError:
            await ctx.send("Command not found")

    @commands.command(hidden=True)
    @commands.check(botadmin)
    async def post (self, ctx, target:discord.User, msg):
        await target.send(msg)


    @commands.command(pass_context=True, hidden=True)
    @commands.check(botadmin)
    async def enable_command(self, ctx, command_name):
        try:
            self.bot.get_command(command_name).enabled = True
            await ctx.send("Command enabled")
        except AttributeError:
            await ctx.send("Command not found")

    @commands.command(hidden=True)
    @commands.check(botadmin)
    async def post_all_emotes(self, ctx, channel_id):
        channel_id = int(channel_id)
        channel = self.bot.get_channel(channel_id)
        await channel.send("Here are all server emotes:")
        for e in channel.guild.emojis:
            if e.animated:
                continue
            msg = await channel.send(("-"*75)+"\n"+e.name+"\n"+e.url)
            await msg.add_reaction('👍')
            await msg.add_reaction('👎')

    @commands.command(hidden=True)
    @commands.check(botadmin)
    async def post_zip_emotes(self, ctx):
        in_memory_zip = BytesIO()
        await ctx.message.attachments[0].save(in_memory_zip)
        with zipfile.ZipFile(in_memory_zip) as zf:
            for n in zf.namelist():
                msg = await ctx.send(("-"*75)+"\n"+n, file=discord.File(zf.open(n), n))
                await msg.add_reaction('👍')
                await msg.add_reaction('👎')

    @commands.command(hidden=True)
    @commands.check(botadmin)
    async def zip_all_emotes(self, ctx):
        in_memory_zip = BytesIO()
        with zipfile.ZipFile(in_memory_zip, mode="w") as zf:
            for e in ctx.guild.emojis:
                async with http().get(e.url) as resp:
                    fname = e.name + ('.png' if e.url.endswith('.png') else '.gif')
                    downloaded = await resp.read()
                    zf.writestr(fname, downloaded)
        in_memory_zip.seek(0, 0)
        await ctx.send("Here are the emotes:", file=discord.File(in_memory_zip, ctx.guild.name + " - emotes.zip"))

    @commands.command(pass_context=True, hidden=True)
    @commands.check(botadmin)
    async def get_command(self, ctx, command_name):
        try:
            await ctx.send("Command enabled" if self.bot.get_command(command_name).enabled else "Command disabled")
        except AttributeError:
            await ctx.send("Command not found")

    @commands.command(pass_context=True, hidden=True)
    @commands.check(botadmin)
    async def af(self, ctx):
        with open('af.png', 'rb') as f:
            await self.bot.get_guild(340547411191660545).edit(icon=f.read())
