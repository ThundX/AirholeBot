from base import Cog, botadmin, make_message_link
import discord
import discord.ext.commands as commands
import json, random

STARRED_FILE = 'starred.txt'
STARBOARD_MSGS = 'starboard.txt'

def isStarReaction(starmotes, emoji):
    return emoji == '⭐' or emoji.id in starmotes

class Starboard(Cog):
    def add_starred_message(self, msg, starboard_msg):
        self.starred_msgs.add(msg.id)
        with open(STARRED_FILE, 'w') as f:
            f.writelines((str(x)+'\n' for x in self.starred_msgs))
        with open(STARBOARD_MSGS, 'a') as f:
            print(starboard_msg.id, file=f)

    def setup(self, config):
        self.threshold = int(config.setdefault('threshold', 2))
        self.starmotes = config.setdefault('starmotes', [])

        try:
            with open(STARRED_FILE) as f:
                self.starred_msgs = set((x.strip() for x in f.readlines()))
        except OSError:
            self.starred_msgs = set()
        try:
            with open(STARBOARD_MSGS) as f:
                self.starboard_messages = [int(x.strip()) for x in f.readlines()]
        except OSError:
            self.starboard_messages = []
    
    @commands.command(hidden=True)
    @commands.check(botadmin)
    async def register_starboard_msgs(self, ctx, starboard_channel_id):
        channel = self.bot.get_channel(int(starboard_channel_id))
        self.starboard_messages = []
        await ctx.send("Start reading starboard")
        c = 0
        with open(STARBOARD_MSGS, 'w') as f:
            async for message in channel.history(limit=None):
                c = c + 1
                print(message.id, file=f)
                self.starboard_messages.append(message.id)
        await ctx.send("Registered {} messages".format(c))

    @commands.command()
    async def randomstarboard(self, ctx):
        if not self.starboard_messages:
            return
        rnd_starboard_message_id = int(random.choice(self.starboard_messages))
        starboard = discord.utils.get(ctx.guild.channels, name='starboard')
        rnd_starboard_message = await starboard.fetch_message(rnd_starboard_message_id)
        await ctx.send(make_message_link(rnd_starboard_message))
        

    @commands.Cog.listener()
    async def on_reaction_add(self, reaction, user):
        message = reaction.message
        if not isStarReaction(self.starmotes, reaction.emoji) or reaction.count < self.threshold or message.id in self.starred_msgs or reaction.message.author == user:
            return
        num_valid_ppl_reacted = 0
        async for reacted_usr in reaction.users():
            if reacted_usr != reaction.message.author:
                num_valid_ppl_reacted += 1
        if num_valid_ppl_reacted < self.threshold:
            print("Not enough people reacted yet...")
            return

        msg_link = make_message_link(message)        
        embed = discord.Embed(description=message.content+'\n[Jump]('+msg_link+')')
        embed.set_author(name=message.author.name, icon_url=message.author.avatar_url)
        embed.set_footer(text=message.created_at.strftime('%a, %B %d, %Y at %H:%M'))
        pics = [a.url for a in message.attachments if any((a.url.casefold().endswith(x) for x in ['.png', '.jpg', '.jpeg', '.gif']))]
        if len(pics)>0:
            embed.set_image(url=pics[0])
        
        starboard = discord.utils.get(message.guild.channels, name='starboard')

        if message.id in self.starred_msgs:
            return
        starboard_msg = await starboard.send('⭐ in {0}'.format(message.channel.mention), embed=embed)
        self.add_starred_message(message, starboard_msg)
