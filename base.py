from discord.ext.commands import Bot
import discord
from discord.ext import commands
import traceback
import sys
import aiohttp
from aiohttp import web
import firebase_admin
from firebase_admin import credentials

keycaps = ['0⃣', '1⃣', '2⃣', '3⃣' , '4⃣', '5⃣', '6⃣', '7⃣', '8⃣', '9⃣']
nsfw_channel_ids = None
botadmin_id = None
session = None
routes = web.RouteTableDef()

def init_firebase(firebase_auth, firebase_dburl):
    cred = credentials.Certificate(firebase_auth)
    firebase_admin.initialize_app(cred, {'databaseURL': firebase_dburl})

def http():
    return session

async def create_session():
    global session
    session = aiohttp.ClientSession()

async def release_session():
    try:
        await session.close()
    except Exception:
        print("There was an error while closing global session object")

def setBotadmin(admin_id: int):
    '''Sets the ID of the bot admin
    
    Arguments:
        admin_id {int or list} -- The ID(s) of the botadmin
    '''

    global botadmin_id
    botadmin_id = admin_id

class Cog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
    
    def setup(self, config):
        pass

def botadmin(ctx):
    '''The given command is only allowed to be used by the bot admin
    
    Example:
        @commands.check(botadmin)
        async def restart(self, ctx):
    '''

    try:
        return ctx.author.id in botadmin_id
    except TypeError:
        return ctx.author.id == botadmin_id

def anyOf(*allowedIDs):
    '''generates a user ID predicate suitable for a commands.check decorator
    
    Example:
        @commands.check(base.anyOf(...))
    '''

    def check_func(ctx):
        return ctx.message.author.id in allowedIDs
    return check_func

def channelAnyOf(*allowedIDs):
    '''generates a channel ID predicate suitable for a commands.check decorator

    Example:
        @commands.check(base.channelAnyOf('123456789'))
        @commands.check(base.channelAnyOf(lambda self: self.valid_channels))
    '''

    if len(allowedIDs)==0:
        raise ValueError("No parameter passed")
    def check_func(ctx):
        if callable(allowedIDs[0]):
            ids = allowedIDs[0](ctx.command.instance)
        else:
            ids = allowedIDs
        return ctx.message.channel.id in ids
    return check_func

def read_f(fname):
    '''Reads a file line by line
    
    Arguments:
        fname {str} -- the filename to open
    
    Returns:
        list -- a list of (stripped) lines in the file
    '''

    with open(fname) as f:
        content = f.readlines()
    content = [x.strip() for x in content] 
    return content

async def post_embed(channel, text, pic_url):
    embed = discord.Embed(description=text)
    if pic_url:
        embed.set_image(url=pic_url)
    await channel.send('', embed = embed)

async def on_command_error(ctx, error):
    if isinstance(error, commands.UserInputError):
        ctx.command.reset_cooldown(ctx)
        await ctx.send(str(error))
        return
    else:
        print('Ignoring exception in command {}'.format(ctx.command), file=sys.stderr)
        traceback.print_exception(type(error), error, error.__traceback__, file=sys.stderr)
    
async def check_url(url:str) -> bool:
    async with http().get(url) as resp:
        return resp.status == 200

def make_message_link(message):
    return 'https://discordapp.com/channels/' + str(message.guild.id) + '/' + str(message.channel.id) + '/' + str(message.id)

def mutex_command(func):
    func.in_process = False

    async def wrapper(self, *args, **kwargs):
        if func.in_process:
            return
        func.in_process = True
        try:
            return await func(self, *args, **kwargs)
        finally:
            func.in_process = False
    func.callback = wrapper
    return wrapper