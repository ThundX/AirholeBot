from base import Cog
import discord.ext.commands as commands

class Pyramids(Cog):

    def setup(self, config):
        self.allowed_pyramid_channels = config.setdefault('allowed_pyramid_channels', [])
        self.pyramid_in_progress = False

    async def do_pyramid(self, ctx, emoji, num, reverse):
        if self.pyramid_in_progress:
            return
        if ctx.message.channel.id not in self.allowed_pyramid_channels:
            return
        
        try:
            self.pyramid_in_progress = True
            if int(num) >= 8:
                return
            for i in range(1, int(num)+1):
                await self.bot.send_message(ctx.message.channel, ''.join((emoji for x in range(i))))
            if reverse:
                for i in range(int(num)+1, 0, -1):
                    await self.bot.send_message(ctx.message.channel, ''.join((emoji for x in range(i))))
        finally:
            self.pyramid_in_progress = False

    @commands.command(pass_context=True)
    @commands.core.cooldown(1, 20, commands.BucketType.user)
    async def pyramid(self, ctx, emoji, num):
        """Pyramids! Usage: pyramid :some_emote: number"""
        await self.do_pyramid(ctx, emoji, num, False)
    
    @commands.command(pass_context=True)
    @commands.core.cooldown(1, 20, commands.BucketType.user)
    async def dpyramid(self, ctx, emoji, num):
        """Pyramids 2.0! Now symmetrical."""
        await self.do_pyramid(ctx, emoji, num, True)