from base import Cog, botadmin, keycaps
import discord.ext.commands as commands

BIGLETTER_START = '🇦'
BIGLETTERS = [chr(ord(BIGLETTER_START) + x) for x in range(26)]
MAPPING_SPECIAL = {
    ' ': '        ',
    '!': '❗',
    '?': '❓'
}

def letter_to_bigletter(l):
    if l in MAPPING_SPECIAL:
        return MAPPING_SPECIAL[l]
    elif ord('a') <= ord(l) <= ord('z'):
        return BIGLETTERS[ord(l) - ord('a')]
    elif ord('0') <= ord(l) <= ord('9'):
        return keycaps[ord(l) - ord('0')]
    else:
        raise commands.errors.BadArgument("Only the characters A-Z, 0-9, ? and ! are supported")
    

class Bigtext(Cog):
    
    def setup(self, config):
        pass

    @commands.command()
    async def dumblex(self, ctx):
        '''Alex is usually dumb'''
        await ctx.send(' '.join([letter_to_bigletter(x) for x in "dumblex"]))

    @commands.command(rest_is_raw=True)
    async def bigtext(self, ctx, *, text=''):
        '''Writes stuff with emoji letters
        
        Arguments:
            text {str} -- The text to write
        '''
        text = text.casefold().strip()
        await ctx.send(' '.join([letter_to_bigletter(x) for x in text]))
