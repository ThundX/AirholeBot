from base import Cog, post_embed
import discord
import discord.ext.commands as commands
import random
import datetime
import asyncio
import re
import aiohttp


class Misc(Cog):

    def setup(self, config):
        self.grouphug_three = config.setdefault('grouphug_three', '')
        self.grouphug_uls = config.setdefault('grouphug_urls', [])
        self.documentary_channel_id = config.setdefault(
            'documentary_channel_id', '')
        self.excluded_id = config.setdefault('savetheme_excluded_role', '')

    @commands.command()
    @commands.core.cooldown(1, 5, commands.core.BucketType.user)
    async def grouphug(self, ctx, *target_usrs: discord.User):
        target_usrs = set(target_usrs)
        if len(target_usrs) == 0:
            return
        if len(target_usrs) == 1:
            await ctx.send('That\'s not a *group*hug, silly!')
            return
        elif len(target_usrs) == 2:
            url = self.grouphug_three
        else:
            url = random.choice(self.grouphug_uls)
        await post_embed(ctx.channel, 'A grouphug!', url)

    @commands.command()
    @commands.has_permissions(administrator=True)
    async def savetheme(self, ctx):
        """Writes a message saving the current theme. Only for admins (Air)"""
        text = datetime.datetime.utcnow().strftime('%a, %B %d, %Y')+'\n'
        text += '\n'.join(('{0} - {1}'.format(x.name, x.nick or x.name) for x in ctx.message.guild.members if not x.bot
                           and all((role.id != self.excluded_id for role in x.roles))))
        await self.bot.get_channel(self.documentary_channel_id).send(text)

    @commands.command()
    async def ping(self, ctx):
        """PING!!"""
        res = (datetime.datetime.utcnow() - ctx.message.created_at).total_seconds() * 1000
        await ctx.send("PONG! ("+str(res) + " ms!)")

    pat_matcher = re.compile('\:.*pat.*\:')

    @commands.Cog.listener()
    async def on_message(self, message):
        if message.author.bot:
            return

        elif '🧀' in message.content:
            await message.channel.send('🧀 Gimme that cheese!! 🧀 *eats*')
        elif '🍪' in message.content:
            await message.channel.send('*Grabs cookie* 🍪🍪🍪 nomnomnom')
            if random.randrange(0, 2) == 0:
                await asyncio.sleep(3)
                await message.channel.send('Here, have one 😳🍪 *hides*')
        elif '🕳' in message.content:
            await message.channel.send('*Jumps in* 🕳 you wanna join me? Come in here 🕳🕳🕳')
        elif 'marry' in message.content and self.bot.user in message.mentions:
            await message.channel.send('Yes')
