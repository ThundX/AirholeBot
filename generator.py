import os
from io import BytesIO

import discord
import discord.ext.commands as commands
from PIL import Image, ImageDraw

from base import Cog, channelAnyOf, keycaps

allowed_channels=[]

class Generator(Cog):

    def setup(self, config):
        self.channels = config['channels']
        if os.path.isdir("generator"):
            names = next(os.walk("generator/"))[1]
            for name in names:
                _, _, image_names = next(os.walk(os.path.join("generator/", name)))
                layers_dict = {}
                for img in image_names:
                    order, variant, _ = img.split(".")
                    variants = layers_dict.setdefault(int(order), [])
                    variants.append(variant)
                
                if any(not isinstance(variants, str) and (len(variants)>len(keycaps) for variants in layers_dict.values())):
                    print("ERROR: More than {} variants".format(len(keycaps)))
                else:
                    self.generate_command(name, layers_dict)

    def cog_check(self, ctx):
        return ctx.message.channel.id in self.channels

    def generate_command(self, name, layers):
        @self.bot.command(name=name)
        @commands.cooldown(1, 20, commands.BucketType.user)
        async def generate_cmd(ctx):
            if ctx.message.channel.id not in self.channels:
                return

            layers_composition = []
            sent_msg = None
            for order in sorted(layers.keys()):
                if isinstance(layers[order], str):
                    layers_composition.append((order, layers[order]))
                elif len(layers[order])==1:
                    layers_composition.append((order, layers[order][0]))
                else:
                    c = 0
                    text = ""
                    for variant in layers[order]:
                        text+="{}. {}\n".format(c, variant)
                        c += 1
                    
                    if sent_msg is None:
                        sent_msg = await ctx.send(text)
                        for add_emote in keycaps+['❌']:
                            await sent_msg.add_reaction(add_emote)
                    else:
                        await sent_msg.edit(content=text)

                    def check(reaction, usr):
                        return usr == ctx.author and reaction.message.id == sent_msg.id and str(reaction.emoji) in keycaps+['❌']
                    
                    reaction, _ = await self.bot.wait_for('reaction_add', timeout = 60.0, check = check)
                    if str(reaction.emoji) == '❌':
                        await sent_msg.delete()
                        return
                    
                    selected_variant = layers[order][keycaps.index(reaction.emoji)]
                    layers_composition.append((order, selected_variant))
            
            img_buf = self.compose_image(name, layers_composition)
            img_buf.seek(0, 0)
            await ctx.send(file=discord.File(img_buf, name+".png"))
            if sent_msg is not None:
                await sent_msg.edit(content=(' '.join((x[1] for x in layers_composition if len(layers[x[0]])>1))))

    def compose_image(self, name, layers_composition):
        def get_img_fname(order, variant):
            return os.path.join("generator", name, "{}.{}.png".format(order,variant))
        
        with Image.open(get_img_fname(*layers_composition[0])) as basepic: # (order,variant)
            current_pic = basepic
            for l in layers_composition[1:]:
                with Image.open(get_img_fname(*l)) as nextpic:
                    current_pic = Image.alpha_composite(current_pic, nextpic)

            buf = BytesIO()
            current_pic.save(buf, "PNG")
            return buf
