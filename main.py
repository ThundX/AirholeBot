import asyncio
import collections
import datetime
import importlib
import inspect
import json
import os
import random
import re
import sys
from concurrent.futures import CancelledError

import aiohttp
import base
import discord
from aiohttp import web
from discord.errors import Forbidden
from discord.ext import commands
from discord.ext.commands.core import BucketType


def case_hook(bot): # because sacro wants case insensitive commands...
    @bot.event
    async def on_message(message):
        if not any((message.content.startswith(p) for p in bot.command_prefix)):
            await bot.process_commands(message) # in not a command process normally
        elif ' ' in message.content:
            cmd, rest = message.content.split(' ', 1)
            message.content = cmd.casefold() + ' ' + rest
            await bot.process_commands(message)
        else:
            message.content = message.content.casefold()
            await bot.process_commands(message)

def setStatus(bot, status):
    @bot.event
    async def on_ready():
        await bot.change_presence(game=discord.Game(name=status))

def setup(config_json):
    token = config_json['token']
    prefixes = tuple(config_json['prefixes'])
    # if unset the botadmin functions will simply not function
    admin_id = config_json.setdefault('admin_id', -1)
    status = config_json.setdefault('status', '')
    configGlobals = config_json.setdefault('globals', dict())
    cogs = config_json.setdefault('cogs', dict())
    
    base.setBotadmin(admin_id)
    config_json['globals'].setdefault('firebase_auth', '')
    if config_json['globals'].setdefault('firebase_dburl', '') != '':
        base.init_firebase(config_json['globals']['firebase_auth'], config_json['globals']['firebase_dburl'])

    bot = commands.Bot(
        cache_auth = False,
        command_prefix=prefixes, 
        help_attrs={'help':'Shows the help message', 'aliases': ['halp']}, pm_help=True)
    bot.config = config_json

    if status:
        setStatus(bot, status)
    case_hook(bot)
    
    bot.add_listener(base.on_command_error)

    for key, val in cogs.items():
        cog_module = importlib.import_module(key)
        try:
            cog_cls=[clsval for clsname, clsval in inspect.getmembers(cog_module, inspect.isclass) if clsname.casefold() == key.casefold()][0]
        except IndexError:
            print("Error importing cog {key}: No class found".format(key=key))
        cog_inst = cog_cls(bot)
        val.update(configGlobals)
        cog_inst.setup(val)
        bot.add_cog(cog_inst)

    return bot

def setup_web(config_json):
    routes = base.routes
    @routes.post('/stop')
    async def stop(request):
        await request.app['bot'].logout()
        return web.Response()
    
    app = web.Application()
    app.add_routes(routes)
    return app

if __name__ == "__main__":
    try:
        config_fname = sys.argv[1] if len(sys.argv) >=2 and sys.argv[1] else 'config.json'
        with open(config_fname) as config_json_file:
            current_config = json.load(config_json_file)
    except FileNotFoundError:
        print("No config.json found. You can generate a template for the config.json file by running make_template.py")
    else:

        bot = setup(current_config)
        loop = bot.loop

        control_server = current_config.get('control_server')
        if control_server:
            app = setup_web(current_config)
            app['bot'] = bot
            runner = web.AppRunner(app)

        try:
            if control_server:
                # schedule server
                loop.run_until_complete(runner.setup())
                port = control_server.get('port', 8081)
                site = web.TCPSite(runner, 'localhost', port)
                loop.run_until_complete(site.start())

            # schedule bot
            loop.run_until_complete(base.create_session())
            loop.run_until_complete(bot.start(current_config['token']))
        except KeyboardInterrupt:
            loop.run_until_complete(bot.logout())
            pending = asyncio.Task.all_tasks(loop=loop)
            gathered = asyncio.gather(*pending, loop=loop)
            try:
                gathered.cancel()
                loop.run_until_complete(gathered)
                gathered.exception()
            except:
                pass
        finally:
            loop.run_until_complete(base.release_session())
            if control_server:
                loop.run_until_complete(runner.cleanup())
            loop.close()
