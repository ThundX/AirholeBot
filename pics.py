from base import Cog, botadmin, http
import discord
import discord.ext.commands as commands
import aiohttp
import google.oauth2.service_account
from google.cloud.storage.client import Client
from google.cloud.storage.blob import Blob
from io import BytesIO
import re
import random

ROO_PREFIX = "roo/"

class Pics(Cog):

    def setup(self, config):
        auth_data = config.setdefault('google_auth', '')
        auth_credentials = google.oauth2.service_account.Credentials.from_service_account_info(
            auth_data)
        self.client = Client(credentials=auth_credentials,
                             project=auth_data.get('project_id'))
        self.bucket = self.client.get_bucket(
            config.setdefault('google_bucket'))

    def get_public_pic(self, msg_content, prefix="botpics/"):
        if msg_content.startswith('.') or msg_content.startswith('/'):
            picname= msg_content[1:].split(".")[0]
            try:
                online_filename = next((x for x in self.do_list_pics(prefix=prefix, casefold=False) if x.split(".")[0].casefold()==picname.casefold()))
            except StopIteration:
                return None, None
            online_full_filename = prefix + online_filename
            print("Downloading pic with full name: "+online_full_filename)
            blob = self.bucket.get_blob(online_full_filename)
            if blob is None:
                print("...but no file with that name was found.")
                return None, None
            downloaded = BytesIO()
            blob.download_to_file(downloaded)
            return downloaded, online_filename
        else:
            return None, None

    def get_srv_emoji(self, msg_content, guild):
        if guild is None:
            return None
        if msg_content.startswith('.') or msg_content.startswith('/'):
            return next((x.url for x in guild.emojis if x.name.casefold() == msg_content[1:].casefold()), None)
        else:
            return None

    @commands.command(hidden=True)
    @commands.cooldown(7, 24*60*60, commands.BucketType.user)
    async def addpic(self, ctx, url, name):
        if not name.endswith(".png") and not name.endswith(".gif"):
            raise commands.errors.BadArgument("Name must end in .png or .gif")
        if not re.fullmatch("[a-zA-Z0-9]+\.[a-zA-Z0-9]+", name):
            raise commands.errors.BadArgument("Name invalid")
        name = name.casefold()
        
        if self.bot.get_command(name.split(".")[0]):
            raise commands.errors.BadArgument("Command names are invalid")
        if name.split(".")[0] in (x.split(".")[0].casefold() for x in self.do_list_pics()):
            raise commands.errors.BadArgument("Image with the same name already exists")

        async with http().get(url) as resp:
            img_content = BytesIO(await resp.read())
            upload_blob = Blob("botpics/"+name, self.bucket)
            upload_blob.upload_from_file(img_content)
            await ctx.send("Picture added ^^")

    @addpic.error
    async def addpic_error(self, error, ctx):
        if isinstance(error, commands.CommandOnCooldown):
            await ctx.send("Cooldown left: "+str(error.retry_after/60/60)+" hours")
    
    def do_list_pics(self, prefix="botpics/", casefold=True):
        def maybe_casefold(s, do_casefold):
            return s.casefold() if do_casefold else s
        return (maybe_casefold(blob.name.split('/')[-1], casefold) for blob in self.bucket.list_blobs(prefix=prefix))

    # @commands.command()
    # @commands.cooldown(1, 300, commands.BucketType.user)
    # async def listpics(self, ctx):
    #     paginator = Paginator()
    #     for line in self.do_list_pics():
    #         paginator.add_line(line)
    #     for page in paginator.pages:
    #         await ctx.message.author.send(page)

    @commands.command()
    async def roondom(self, ctx):
        selected = random.choice(list(self.do_list_pics(prefix=ROO_PREFIX, casefold=False)))
        to_send, new_filename = self.get_public_pic("."+selected, prefix=ROO_PREFIX)
        if to_send is None:
            return
        to_send.seek(0, 0)
        await ctx.send(file=discord.File(to_send, new_filename))

    @commands.command()
    async def randompic(self, ctx):
        selected = random.choice(list(self.do_list_pics(casefold=False)))
        to_send, new_filename = self.get_public_pic("."+selected)
        to_send.seek(0, 0)
        await ctx.send(file=discord.File(to_send, new_filename))

    @commands.Cog.listener()
    async def on_message(self, message):
        if message.author == self.bot.user:
            return
        to_send, new_filename = self.get_public_pic(message.content)
        if not to_send:
            new_filename = "img.png"
            to_send = self.get_srv_emoji(
                message.content, message.guild)
            if to_send:
                await message.channel.send(to_send)
            return
        else:
            to_send.seek(0, 0)
            await message.channel.send(file=discord.File(to_send, new_filename))
