import random
from io import BytesIO

import discord
import discord.ext.commands as commands
from PIL import Image, ImageDraw

from base import Cog

class Colour(Cog):

    def setup(self, config):
        pass
    
    def formathex(self, colour):
        return '#%02x%02x%02x' % colour

    def fillcolour(self, colour):
        with Image.new("RGB", (100, 100)) as img:
            draw = ImageDraw.Draw(img)
            draw.rectangle((0, 0, 100, 100), fill=colour)
            buf = BytesIO()
            img.save(buf, "PNG")
            return buf

    @commands.command(hidden=True)
    async def randomcolour(self, ctx):
        colour = (random.randrange(0, 256), random.randrange(0, 256), random.randrange(0, 256))
        with self.fillcolour(colour) as buf:
            buf.seek(0, 0)
            await ctx.send(self.formathex(colour), file=discord.File(buf, "image.png"))
