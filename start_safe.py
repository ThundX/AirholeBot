import subprocess, time, sys, os

c = 0
if len(sys.argv) > 1:
    pyfile = sys.argv[1]
else:
    pyfile = 'main.py'
dir, pyexec = os.path.split(pyfile)

while True:
    c+=1
    print("Starting instance " + str(c) + "("+pyfile+")")
    subprocess.run(["python3.5 "+pyexec], shell=True, cwd=dir or None)
    time.sleep(30)
