from base import Cog
import discord
import discord.ext.commands as commands
import random, datetime, asyncio, re
import aiohttp
from PIL import Image
from io import BytesIO
import makepic

class GenPics(Cog):

    def setup(self, config):
        pass

    @commands.command(rest_is_raw=True)
    @commands.cooldown(1, 15, commands.BucketType.user)
    async def fluffsays(self, ctx, *, text=''):
        if('<@' in text or '<#' in text):
            raise commands.BadArgument("Mentions don't work yet... sorry.")

        print(ctx.message.content)
        text = text or '...!'
        print("Executing genpic command with text: "+text)
        
        try:
            with makepic.make_pic('fluff', text) as buf:
                buf.seek(0, 0)
                await ctx.send(file=discord.File(buf, "image.png"))
        except makepic.TextTooBigException:
            print("Text is too big...")
            raise commands.BadArgument("The text is too big for the textbox. We don't want it to look buggy now, do we?")

    @commands.command(rest_is_raw=True)
    @commands.cooldown(1, 15, commands.BucketType.user)
    async def chockensays(self, ctx, *, text=''):
        if('<@' in text or '<#' in text):
            raise commands.BadArgument("Mentions don't work yet... sorry.")

        print(ctx.message.content)
        text = text or '...!'
        print("Executing genpic command with text: "+text)
        
        try:
            with makepic.make_pic('chocken', text) as buf:
                buf.seek(0, 0)
                await ctx.send(file=discord.File(buf, "image.png"))
        except makepic.TextTooBigException:
            print("Text is too big...")
            raise commands.BadArgument("The text is too big for the textbox. We don't want it to look buggy now, do we?")

