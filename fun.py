from base import Cog, post_embed, http, botadmin, routes
import discord.ext.commands as commands
import json, random, secrets, asyncio
import requests
from aiohttp import web
import firebase_admin
from firebase_admin import db

class Fun(Cog):

    def load_single_command(self, cmd_name, json_cmd):
        cooldown_sec = int(json_cmd.get('cooldown', 5))

        self.bot.remove_command(cmd_name)
        
        @self.bot.command(name=cmd_name, aliases = list(json_cmd.get('aliases', [])))
        @commands.core.cooldown(1, cooldown_sec, commands.core.BucketType.user)
        async def post_random_pic(ctx, *args):
            url = random.choice(ctx.command.rnd_urls) if ctx.command.rnd_urls else ""
            try:
                if ctx.command.type == 'text':
                    text_to_send = ctx.command.message_text
                    if not '{url}' in text_to_send:
                        text_to_send += '\n{url}'
                    formatted = text_to_send.format(*args, url=url, caller=ctx.message.author.mention)
                    formatted = formatted.replace('@everyone', '[@]everyone').replace('@here', '[@]here')
                    sent_msg = await ctx.send(formatted)
                elif ctx.command.type == 'embed':
                    text = ctx.command.message_text.replace('{url}', '').strip()
                    formatted = text.format(*args, url=url, caller=ctx.message.author.mention)
                    sent_msg = await post_embed(ctx.channel, formatted, url)
            except IndexError:
                await ctx.send("You have the wrong number of people mentioned...")

        if json_cmd.get('nsfw', False):
            commands.is_nsfw()(post_random_pic)
        
        post_random_pic.rnd_urls=json_cmd.get('urls', [])
        post_random_pic.message_text=json_cmd.get('text', '{url}') or '{url}'
        post_random_pic.enabled = json_cmd.get('enabled', True)
        post_random_pic.type = json_cmd.get('type', 'text')

        self.img_commands.append(post_random_pic)

    def load_command_set(self, url):
        json_commands = requests.get(url).json()
        if json_commands is None:
            return

        for cmd_name in json_commands:
            json_cmd = json_commands[cmd_name]
            self.load_single_command(cmd_name, json_cmd)

    def reload(self):
        for cmd in self.img_commands:
            self.bot.remove_command(cmd.name)
        
        self.img_commands = []
        self.load_command_set(self.config_url)
        self.load_command_set(self.config_url2)

    def setup(self, config):
        self.privileged = config.setdefault('privileged', [])
        routes.post('/request/img')(self.request_add_img)
        self.config_url = config.setdefault('config_url')
        self.config_url2 = config.setdefault('config_url2')
        self.img_commands = []
        self.firebase_auth = config.setdefault('firebase_auth')
        self.firebase_dburl = config.setdefault('firebase_dburl')
        self.reload()
    
    @commands.command()
    @commands.check(botadmin)
    async def reload_pics(self, ctx):
        self.reload()
        await ctx.send("Pics reloaded!")

    async def wait_for_confirm(self, token, data):
        def check_func(m):
            #return m.author.id in self.privileged and m.content == token
            return m.content == token
        try:
            msg = await self.bot.wait_for('message', timeout=120, check = check_func)
            new_cmd_name = str(data['name']).casefold()
            await msg.channel.send("Confirmed picture add request for command {0}!".format(new_cmd_name))
            try:
                del data['name']
                if data['type'] not in ['text', 'embed']:
                    data['type'] = 'embed'
                db.reference('img_commands2').child(new_cmd_name).set(data)
                self.load_single_command(new_cmd_name, data)
            except Exception as ex:
                await msg.channel.send("Something went wrong... Here's the error message: \n"+str(ex))
            else:
                await msg.channel.send("Successfully added command {0}".format(new_cmd_name))
        except asyncio.TimeoutError:
            print("Pic add request timeouted")

    async def request_add_img(self, request):
        data = await request.json()
        if not data['name'] or not data['name'].isalnum():
            raise web.HTTPBadRequest()
        token = secrets.token_urlsafe(8) # a reasonably short token
        
        # correct order of events is:
        # generate token -> register wait for token -> send token back to user
        task = asyncio.ensure_future(self.wait_for_confirm(token, data))
        return web.Response(text=token)

    
