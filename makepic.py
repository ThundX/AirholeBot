from PIL import Image, ImageDraw, ImageFont
import textwrap
from io import BytesIO
from collections import namedtuple
import random

imgdata = namedtuple("imgdata", "name tw coords fontsize font size color align")

def new_img(name, tw, coords, fontsize, font="res/FreeSans.ttf", size=None, color=(255, 255, 255), align='l'):
    return imgdata(name=name, tw=tw, coords=coords, fontsize=fontsize, font=font, size=size, color=color, align=align)

all_img = {
    "fluff": [new_img(name="res/fluffsays.jpg", tw=38, coords=(24, 894), font="res/FreeSans.ttf", fontsize=50, size=(1868, 164))], \
    "chocken": [new_img(name="res/chockensays.png", tw=38, coords=(111, 55), font="res/adamwarrenpro.ttf", fontsize=50, size=(305, 115), color=(0,0,0), align='c')]
    }

def calc_coords(base_coords, box_size, text_size, align):
    w, h = text_size
    if w > box_size[0] or h > box_size[1]:
        return None
    newy = base_coords[1]
    if align=='l':
        newx=base_coords[0]
    elif align=='c':
        newx = base_coords[0] + (box_size[0]-w)/2
    else:
        newx = base_coords[0] + box_size[0] - w
    return (newx, newy)

class TextTooBigException(Exception):
    pass

def make_pic(imgtype, text):
    text = text.strip()
    img = random.choice(all_img[imgtype])
    #if '\n' not in text:
    #    text = '\n'.join(textwrap.wrap(text, img.tw)) # autowrap if not already present
    #fontsize = img.fontsize if len(text)>5 else img.fontsize*2
    fontsize = img.fontsize
    fnt = ImageFont.truetype(img.font, fontsize)
    with Image.open(img.name) as pic:
        draw = ImageDraw.Draw(pic) 
        coords = img.coords
        if img.size is not None:
            text_size = draw.textsize(text, font=fnt)
            coords = calc_coords(coords, img.size, text_size, img.align)
            if coords is None:
                raise TextTooBigException()
            
        draw.text(coords, text, font=fnt, fill=img.color)
        buf = BytesIO()
        pic.save(buf, "PNG")
        return buf
