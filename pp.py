from base import Cog, http
import discord
import discord.ext.commands as commands
import random, datetime, asyncio, re
import aiohttp
from PIL import Image
from io import BytesIO

def generate_cc():
    m = 0.4
    rnd = random.expovariate(m)*100
    return int(min(max(rnd,0),399))

class PP(Cog):

    def setup(self, config):
        self.target_pic = config.setdefault('target_pic', '')
        self.avatar_coords = config.setdefault('avatar_coords', [])
        self.checking_text = config.setdefault('checking_text', '')
        self.identify_text = config.setdefault('identify_text', '')
        self.crime_text = config.setdefault('crime_text', '')
        self.no_crime_text = config.setdefault('no_crime_text', '')
        self.crime_coefficient = config.setdefault('crime_coefficient', '')
        self.average_text = config.setdefault('average_text', '')

    async def generate_pic(self, ctx, target:discord.User):
        if target.avatar_url:
            async with http().get(target.avatar_url) as resp:
                with Image.open(BytesIO(await resp.read())) as avatar_pic:
                    with Image.open(self.target_pic) as target_pic:
                        frames=[]
                        resized = avatar_pic.resize((100,100))
                        num_frames = 0
                        try:
                            while True:
                                new_target = target_pic.convert('RGB')
                                new_coords = (209+random.randint(-25,25),29+random.randint(-25,25))
                                new_target.paste(resized, new_coords)
                                frames.append(new_target)
                                target_pic.seek(target_pic.tell()+1)
                                num_frames+=1
                        except EOFError:
                            pass

                        buffer = BytesIO()
                        frames[0].save(buffer, "GIF", save_all=True, append_images=frames[1:])
                        buffer.seek(0, 0)
                        return buffer
    
    async def tell_wait(self, ctx, txt):
        msg = await ctx.send(txt)
        for i in range(5):
            await asyncio.sleep(0.5)
            txt = txt+"."
            await msg.edit(content=txt)

    @commands.command(hidden=True)
    @commands.cooldown(1, 300, commands.BucketType.user)
    async def psychopass(self, ctx, target:discord.User):
        checking_text = self.checking_text.format(target.mention)
        buffer = (await asyncio.gather(self.generate_pic(ctx, target), self.tell_wait(ctx, self.identify_text)))[0]
        with buffer:
            await ctx.send(file=discord.File(buffer, "res/target.gif"))
        await asyncio.sleep(2)
        await self.tell_wait(ctx, checking_text)
        cc = generate_cc()
        await ctx.send(self.crime_coefficient.format(cc))
        if cc < 100:
            await ctx.send(self.no_crime_text.format(target.mention))
        elif cc < 300:
            await ctx.send(self.average_text.format(target.mention))
        else:
            await ctx.send(self.crime_text.format(target.mention))