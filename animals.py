from base import Cog, check_url, http, post_embed
import discord
import discord.ext.commands as commands
import aiohttp
import praw

class Animals(Cog):

    def setup(self, config):
        client_id = config.setdefault('client_id', '')
        client_secret = config.setdefault('client_secret', '')
        user_agent = config.setdefault('user_agent', '')

        self.reddit = praw.Reddit(client_id=client_id,
                     client_secret=client_secret,
                     user_agent=user_agent)
        self.subreddit = self.reddit.subreddit('cutebunnies')

    @commands.command(aliases=['rabbit'])
    @commands.cooldown(1, 8, commands.BucketType.user)
    async def bunny(self, ctx):
        for i in range(4): # 4 tries
            rnd_url = self.subreddit.random().url
            if (await check_url(rnd_url)) and any((rnd_url.endswith(x) for x in ['.jpg', '.jpeg', '.png', '.gif', '.gifv', '.bmp'])):
                await post_embed(ctx.channel, '', rnd_url)
                return
        await ctx.send("Sorry, no bunny pics found for now. ごめん　🙇")
       

    @commands.command() 
    @commands.cooldown(1, 8, commands.BucketType.user)
    async def cat(self, ctx):
        async with http().get('http://thecatapi.com/api/images/get?format=src') as response:
            if await check_url(response.url):
                await post_embed(ctx.channel, '', response.url)
            else:
                await ctx.send("Sorry, no cat pics found for now. ごめん　🙇")

    @commands.command()
    @commands.cooldown(1, 8, commands.BucketType.user)
    async def dog(self, ctx):
        async with http().get('https://dog.ceo/api/breeds/image/random') as response:
            url = (await response.json())['message']
            if await check_url(url):
                await post_embed(ctx.channel, '', url)
            else:
                await ctx.send("Sorry, no dog pics found for now. ごめん　🙇")

    @commands.command(aliases=['birb'])
    @commands.cooldown(1, 8, commands.BucketType.user)
    async def bird(self, ctx):
       async with http().get('https://birdsare.cool/bird.json?only=png,gif,jpg') as response:
            url = (await response.json())['url']
            if await check_url(url):
                await post_embed(ctx.channel, '', url)
            else:
                await ctx.send("Sorry, no bird pics found for now. ごめん　🙇")
