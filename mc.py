from base import Cog, botadmin
import discord
import random
from discord.ext import commands
from libs.async_mcrcon import MinecraftClient

class MC(Cog):
    """Minecraft RCON commands."""

    def setup(self, config):
        self.addr = config['addr']
        self.port = config['port']
        self.pwd = config['pwd']

    @commands.command(hidden=True)
    @commands.check(botadmin)
    async def rcon(self, ctx, command):
        """Sends an RCON command"""
        async with MinecraftClient(self.addr, self.port, self.pwd) as mc:
            output = await mc.send(command)
            to_send = ("MC: " + str(output))[:1900]
            await ctx.send(to_send)
    
    @commands.command()
    @commands.cooldown(1, 5, commands.BucketType.user)
    async def mcplayers(self, ctx):
        """Shows the players currently logged into MC"""
        async with MinecraftClient(self.addr, self.port, self.pwd) as mc:
            output = await mc.send("list")
            to_send = (str(output))[:1900]
            await ctx.send(to_send)
            